def is_adult(age):
    if age >= 18:
        return True
    else:
        return False

if __name__ == '__main__':
    print(is_adult(18))


def return_words_containing_letter_a(word_list):
    return [word for word in word_list if 'a' in word.lower()]


