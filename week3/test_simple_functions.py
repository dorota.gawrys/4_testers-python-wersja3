from simple_functions import is_adult
from simple_functions import  return_words_containing_letter_a


def test_is_adult_for_age_greater_than_18():
    assert is_adult(19)
    # assert zawsze oczekuje że po prawej stronie jest " True" więc przy testach assert nie trzeba dawać True


def test_is_adult_if_age_18():
    assert is_adult(18) == True


def test_is_adult_for_age_lower_than_18():
    assert is_adult(16) == False

    # można też zamienić asserta; ssert is_adult(16) == False na : assert not is_adult bez False


def test_list_containing_all_words_with_letter_a():
    input_list = ["Parrot", "Cat", "Any", "Amazonia", "Papaya"]
    expected_list = ["Parrot", "Cat", "Any", "Amazonia", "Papaya"]
    assert return_words_containing_letter_a(input_list) == expected_list


def test_list_containing_mixed_words():
    input_list = ["Parrot", "Bye", "Any", "Amazonia", "Papaya", "Miro", "why", "alabama"]
    expected_list = ["Parrot", "Any", "Amazonia", "Papaya", "alabama"]
    assert return_words_containing_letter_a(input_list) == expected_list


def test_list_not_containing_words_with_a():
    input_list = ["Bye", "Miro", "why"]
    expected_list = []
    assert return_words_containing_letter_a(input_list) == expected_list


def test_empty_list():
    assert return_words_containing_letter_a([]) == []

