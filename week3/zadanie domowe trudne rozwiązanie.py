import random
import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


# 1. Funkcja do generowania słownika
def generate_dictionary(is_female):
    if is_female:
        first_name = random.choice(female_fnames)
    else:
        first_name = random.choice(male_fnames)
    last_name = random.choice(surnames)
    email = f"{first_name.lower()}.{last_name.lower()}@example.com"

    current_year = datetime.datetime.now().year
    age = random.randint(5, 45)
    birth_year = current_year - age
    is_adult = age >= 18

    return {
        'firstname': first_name,
        'lastname': last_name,
        'email': email,
        'age': age,
        'adult': is_adult,
        'birth_year': birth_year,
        'country': random.choice(countries),
    }


# 2. Funkcja do wygenerowania listy słowników
def generate_list_of_dictionaries(number_of_male, number_of_female):
    output_list = []
    for i in range(number_of_male):
        output_list.append(generate_dictionary(False))
    for i in range(number_of_female):
        output_list.append(generate_dictionary(True))
    return output_list


def generate_description_for_person_dictionary(person_dictionary):
    return f"Hi! I'm {person_dictionary['firstname']} {person_dictionary['lastname']}. I come from {person_dictionary['country']} and I was born in {person_dictionary['birth_year']}"


if __name__ == '__main__':
    random_people_list = generate_list_of_dictionaries(5, 5)
    print(random_people_list)
    for element in random_people_list:
        print(generate_description_for_person_dictionary(element))