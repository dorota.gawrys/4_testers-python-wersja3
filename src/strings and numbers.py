first_name = "Dorota"
last_name = "Gawrys"
email = "dorota.gawrys@gmail.com"

print("Mam na imię", first_name, ".", )

my_bio = "Mam na imię" + " "+ first_name +". Moje nazwisko to"+ " " + last_name +"."
print(my_bio)

#F-string
my_bio_using_f_string = (f"Mam na imię {first_name}.\nMoje nawisko to {last_name}.\nMój emial to {email}")
print(my_bio_using_f_string)

###Algebra###

circle_radius = 20
area_of_a_circle_with_redius_5 = 3.14 * circle_radius * circle_radius
print(f"Area of circle with radius {circle_radius}:",area_of_a_circle_with_redius_5)

long_mathematical_expression = (2 + 3 + 5 * 7 / 3 + 7**3)
print(long_mathematical_expression)




