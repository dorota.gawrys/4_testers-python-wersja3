import random
import datetime

today = datetime.date.today()
year = today.year

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Przykład jak można losować imię z listy
random_female_firstname = random.choice(female_fnames)
print(random_female_firstname)

# Przykład jak można losować wiek z liczb całkowitych od 1 do 65
random_age = random.randint(1, 45)
print(random_age)

# Przykładowy wygenerowany pojedynczy słownik
example_dictionary = {
    'firstname': 'Kate',
    'lastname': 'Yu',
    'email': 'kate.yu@example.com',
    'age': 23,
    'country': 'Poland',
    'adult': True
}


def check_if_adult(age):
    if age >= 18:
        return True
    else:
        return False


if __name__ == '__main__':
    print(check_if_adult(30))


def check_birth_year(input_age):
    today = datetime.date.today()
    year = today.year
    birth_date = year - input_age
    return birth_date


if __name__ == '__main__':
    print(check_birth_year(34))


def generate_random_female_person_dictionary():
    first_name = random.choice(female_fnames)
    last_name = random.choice(surnames)
    email_formated = f"{first_name.lower()}.{last_name.lower()}@example.com"

    return {
        'firstname': first_name,
        'lastname': last_name,
        'email': email_formated,
        'age': random.randint(5, 45),
        'country': random.choice(countries),

    }


if __name__ == '__main__':
    for person in range(5):
        print(generate_random_female_person_dictionary())


def generate_list_of_random_female_person_dictionaries(number_of_dictionaries):
    return [generate_random_female_person_dictionary() for i in range(number_of_dictionaries)]


if __name__ == '__main__':
    print(generate_list_of_random_female_person_dictionaries(5))


def generate_random_male_person_dictionary():
    email_formated = f"{first_name.lower()}.{last_name.lower()}@example.com"
    first_name = random.choice(male_fnames)
    last_name = random.choice(surnames)
    return {
        'firstname': first_name,
        'lastname': last_name,
        'email': email_formated,
        'age': random.randint(1, 45),
        'country': random.choice(countries),

    }


if __name__ == '__main__':
    for person in range(5):
        print(generate_random_male_person_dictionary())


def generate_list_of_random_male_person_dictionaries(number_of_dictionaries):
    return [generate_random_male_person_dictionary() for i in range(number_of_dictionaries)]


if __name__ == '__main__':
    male_output_list = (generate_list_of_random_male_person_dictionaries(5))
    print(male_output_list)


def marge_to_one_random_list_for_two_gender():
    list_male = generate_list_of_random_male_person_dictionaries(5),
    list_female = generate_list_of_random_female_person_dictionaries(5),
    one_random_list_for_two_gender = list_female + list_male
    print(one_random_list_for_two_gender)

# do tego momentu działa
