import random
import string

for i in range(1):
    # get random string of length 6 without repeating letters
    result_str = ''.join(random.sample(string.ascii_lowercase, 8))


def create_credentials_for_email(email):
    return {
        "email": email,
        "password": result_str
    }


if __name__ == '__main__':
    new_credentials = create_credentials_for_email("dorota.gawrys@gmail.com")
    print(new_credentials)


def check_if_conditions_normal(Celcius, Hektopascal):
    if Celcius == 0 and Hektopascal == 1012:
        return True
    else:
        return False


if __name__ == '__main__':
    conditions = check_if_conditions_normal(10, 1011)
    print(conditions)

    print(f"is that normal conditions {check_if_conditions_normal(10, 1011)}")


def check_grades_for_score(percentage):
    if percentage >= 90:
        return 5
    elif percentage >= 75:
        return 4
    elif percentage >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print(f"What grade for for 60 percentage is {check_grades_for_score(32)}")


def print_number_from_0_to_limit(limit):
    for i in range(limit + 1):
        print(i)


if __name__ == '__main__':
    print_number_from_0_to_limit(20)


def print_numbers_devided_by_7(from_number, to_number):
    for i in range(from_number, to_number + 1):
        if i % 7 == 0:
            print(i)


if __name__ == '__main__':
    print_numbers_devided_by_7(1, 70)


def print_each_temperatures_in_celsius_and_fahrenheit(temperatures_in_celsius_list):
    for temp in temperatures_in_celsius_list:
        print(f"Temp in celsius is {temp}, tempretures in Fahrenheit is {round(9 / 5 * temp + 32)}.")


if __name__ == '__main__':
    temperatures_in_celsius_list = [10, 34, 21, 23]
    print_each_temperatures_in_celsius_and_fahrenheit(temperatures_in_celsius_list)


def convert_celsius_to_fahrenheit(celcius):
    fahrenheit = 9 / 5 * (celcius) + 32
    return fahrenheit


if __name__ == '__main__':
    print(f"Fahrenheit is {convert_celsius_to_fahrenheit(10)}")


def convert_list_of_temp_to_fahrenheit(list_of_temp_in_celsius):
    fahrenheit_temp = []
    for celcius in list_of_temp_in_celsius:
        fahrenheit = convert_celsius_to_fahrenheit(celcius)
        fahrenheit_temp.append(fahrenheit)
    return fahrenheit_temp


if __name__ == '__main__':
    list_of_temp_in_celsius1 = [10, 20, 23]
    print(convert_list_of_temp_to_fahrenheit(list_of_temp_in_celsius1))


def print_gamer_description(dictionary_of_gamer):
    player_description = f"Witaj player {dictionary_of_gamer["nick"]} is of type {dictionary_of_gamer["type"]} and has {dictionary_of_gamer["exp"]} EXP"
    print(player_description)


if __name__ == '__main__':
    player_description1 = {
        "nick": "Doris",
        "type": "Mistrz",
        "exp": 2000
    }
print_gamer_description(player_description1)


def find_number_divisible_by_number_in_range(number_from, number_to, divisible_by):
    list_of_num = []
    for number in range(number_from, number_to + 1):
        if number % divisible_by == 0:
            list_of_num.append(number)
    return list_of_num


if __name__ == '__main__':
    print(find_number_divisible_by_number_in_range(1, 100, 10))

def get_words_reversed(list_of_words):
    reversed_word = []
    return [word[::-1] for word in list_of_words]


if __name__ == '__main__':
    list_of_words1 = ["Dorota", "dzik", "ozon"]
    print(get_words_reversed(list_of_words1))
    print(get_words_reversed(["dzik","jablko"]))
