if __name__ == '__main__':

    first_name = "Janusz"
    last_name = "Nowak"
    second_name = "Barbara"
    second_last_name = "Kowalska"

    email = first_name.lower() + "." + last_name.lower() + "@4testers.pl"

    print(email)
    email2 = second_name.lower() + "." + second_last_name.lower() + "@4testers.pl"

    print(email2)

    email_formatted = f"{first_name.lower()}.{last_name.lower()}@4testers.pl"
    print(email_formatted)

    email_formatted2 = f"{second_name.lower()}.{second_last_name.lower()}@4testers.pl"
    print(email_formatted2)



def email_address_generator(name,surname):
    return f"{name}.{surname}@testers.pl"

if __name__ == '__main__':
    email_address_of_user_man = email_address_generator("Janusz","Nowak")
    email_address_of_user_woman = email_address_generator("Barbara","Kowalska")
    print(email_address_of_user_man)
    print(email_address_of_user_woman)


def generate_email(first_name, last_name):
        return f"{first_name.lower()}.{last_name.lower()}@4testers"

janusz_email = generate_email("Janusz", "Nowak")
barbara_email = generate_email("Barbara", "Kowalska")
print(janusz_email)
print(barbara_email)