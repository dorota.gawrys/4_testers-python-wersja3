from faker import Faker
import random

fake = Faker()


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(),
        "seniority_in_years": random.randint(1, 40),
        "female": random.choice([True, False])
    }


def generate_array_of_employee_dictionaries(number_of_dictionaries):
    return [generate_random_employee_dictionary() for i in range(number_of_dictionaries)]


def get_emails_of_employees_with_seniority_years_greater_than_10(list_of_employees):
    output_emials = []

    for employee_dictionary in list_of_employees:
        if employee_dictionary["seniority_years"] > 10:
            output_emials.append(employee_dictionary["email"])


if __name__ == '__main__':
    for n in range(100):
        print(generate_random_employee_dictionary())

    print(generate_array_of_employee_dictionaries(20))

    generate_employees = generate_array_of_employee_dictionaries(20)
    print(generate_employees)

    #ta ostatnia część nie działa, sprawdzić z prezentacji
