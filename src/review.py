def calculate_square_of_number(number):
    return number ** 2


def volume_of_cuboid(a, b, c):
    return a * b * c


def convert_celcius_to_fahrenheit(grade):
    return grade * 1.8 + 32


if __name__ == '__main__':
    fahrenheit_from_celcius = convert_celcius_to_fahrenheit(20)
    print(fahrenheit_from_celcius)


def create_greetings(name, city):
    greeting_format = f"Witaj {name.upper()}.Miło Cię widzieć w mieście {city.upper()}."
    print(greeting_format)


if __name__ == '__main__':
    create_greetings("Michał", "Toruń")
    create_greetings("Beata", "Kołobrzeg")

if __name__ == '__main__':
    cuboid_lessons = volume_of_cuboid(3, 5, 7)
    print(cuboid_lessons)

if __name__ == '__main__':
    square_of_number = calculate_square_of_number(5)
    square_of_number1 = calculate_square_of_number(5.3)
    square_of_number2 = calculate_square_of_number(.09)
    print(square_of_number1)

if __name__ == '__main__':
    list_of_family = ["Michal", "iwona", "Basia", "Kazik"]
    lenght_of_family = len(list_of_family)
    print(lenght_of_family)
    print(list_of_family)
    print(list_of_family[0])
    list_of_family.append("Dorota")
    print(list_of_family)
    print(f"Moja rodzina to{list_of_family}")

if __name__ == '__main__':
    emails = ["a@example.com", "b@example.com"]
    lenght_of_emails_list = len(emails)
    print(lenght_of_emails_list)
    print(emails[0])
    print(emails[-1])
    emails.append("cde@example.com")
    print(emails)


def create_email(name, surname):
    email_format = f"{name.lower()}.{surname.lower()}@4testers.pl"
    print(email_format)


if __name__ == '__main__':
    create_email("Janusz", "Nowak")
    create_email("Barbara", "gawrys")


def calculate_number_3times(number1, number2, number3):
    return [number1 * 3, number2 * 3, number3 * 3]


if __name__ == '__main__':
    result_of_numbers = calculate_number_3times(3, 5, 8)
    print(result_of_numbers)


def calculate_average_of_grades(list_of_grade):
    result = sum(list_of_grade) / len(list_of_grade)
    rounded_result = round(result, 1)
    return rounded_result


if __name__ == '__main__':
    list_of_grade = [3, 5, 6, 2, 1, 2, 3, 4]
    print(f"Średnia ocen to {calculate_average_of_grades(list_of_grade)}")

if __name__ == '__main__':

    dictionary_of_my_friend= {
        "name": "ewe",
        "surname": "Baran",
        "hobbies": ["sleeping","photography"],
        "age": 38
    }

    print(dictionary_of_my_friend)
    dictionary_of_my_friend["name"] = "Bartek"
    dictionary_of_my_friend["age"] += 1
    dictionary_of_my_friend["hobbies"].append("sun")
    print(dictionary_of_my_friend)


