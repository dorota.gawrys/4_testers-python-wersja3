from faker import Faker
import random

fake = Faker()


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(),
        "seniority_years": random.randint(1, 60),
        "female": random.choice([True, False])
    }


if __name__ == '__main__':
    for employee in range(100):
        print(generate_random_employee_dictionary())


def generate_list_of_employee_dictionaries(number_of_dictionaries):
    return [generate_random_employee_dictionary() for i in range(number_of_dictionaries)]


if __name__ == '__main__':
    print(generate_list_of_employee_dictionaries(20))


def generate_list_of_emails_employees_seniority_more_than_10(list_of_employees):
    output_emails = []
    for employee_dictionary in list_of_employees:
        if employee_dictionary["seniority_years"] > 10:
            output_emails.append(employee_dictionary["email"])
    return output_emails


if __name__ == '__main__':

    generated_employees = generate_list_of_employee_dictionaries(20)
    print(generated_employees)

    filtered_emails = generate_list_of_emails_employees_seniority_more_than_10(generated_employees)
    print(filtered_emails)