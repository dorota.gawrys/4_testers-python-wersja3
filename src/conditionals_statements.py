def print_temperaturedescription(tempretures_in_celcius):
    print(f"Temperature right now is {tempretures_in_celcius} Celcius Degrees")
    if tempretures_in_celcius > 25:
        print("It is getting hot")
    elif tempretures_in_celcius > 0:
        print("Its okey")
    else:
        print("Its getting cold")


def is_person_adult(age):
    if age >= 18:
        return True
    else:
        return False


if __name__ == '__main__':
    tempretures_in_celsius = -10
    print_temperaturedescription(tempretures_in_celsius)

    age_kate = 17
    age_tom = 18
    age_marta = 21

    print("Kate", is_person_adult(age_kate))
