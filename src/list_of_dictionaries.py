animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": "5",
    "male": False
}

animals = ["dog", "cat", "fish"]

animals4 = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "cat",
        "age": 5,
        "male": False
    },
    {
        "kind": "fish",
        "age": 1,
        "male": True
    }
]

print(len(animals))
print(animals[0])
print(animals[-1])

last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("The age of last animal is", last_animal_age)

print(animals[0]["male"])
print(animals[-1]["kind"])

animals.append(
    {
        "kind": "zebra",
        "age": 8,
        "male": False
    }
)
print(animals)