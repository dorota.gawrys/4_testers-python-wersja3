def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)

print_given_number_multiplied_by_3(4)
print_given_number_multiplied_by_3(5)
print_given_number_multiplied_by_3(8)


def multiplyList(myList):
    # Multiply elements one by one
    result = 1
    for x in myList:
        result = result * x
    return result


# Driver code
list1 = [4, 5, 8]
print(multiplyList(list1))

def make_list_of_triple_numbers(number1, number2, number3):
    return [number1 * 3, number2 * 3, number3 * 3]


    print(make_list_of_triple_numbers)