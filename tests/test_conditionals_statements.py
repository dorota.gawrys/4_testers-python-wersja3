from src.conditionals_statements import is_person_adult


def test_age_over_18_years_old_should_be_adult():
    is_adult = is_person_adult(19)
    assert is_adult == True


def test_age_18_years_old_should_be_adult():
    is_adult = is_person_adult(18)
    assert is_adult


def test_age_under_18_years_old_should_be_adult():
    is_adult = is_person_adult(16)
    assert not is_adult
